#Database Lookup#

Database query tool built in c# using framework 4.0. Very small, lightweight database query tool.

![dblookup.png](https://bitbucket.org/repo/6d757z/images/293357160-dblookup.png)

This application was originally built as a support tool to assist when deploying .net applications when tools like Management Studio are not available. 

Some of the functions are listed below:

* Support for SQLConnection and OleDbConnecton. This is automatic selected depending if the 'provider' is set in the connectionstring.
* Pre-populated connection strings if connectstring.txt is present and populated.
* Full automatic logging of all statement executions.
* Ability to only execute highlighted T-SQL.
* Database list view for MS SQL Server databases.
* Ability to generate SELECT, UPDATE, INSERT and DELETE statements. available from list view right click menu.
* Exception handerling to the query result window.
* File export options from the query results window.
* Sortable columns for the query results window.

![dblookup_statements.png](https://bitbucket.org/repo/6d757z/images/3836398427-dblookup_statements.png)

Example of list view right click menu

![dblookup_export.png](https://bitbucket.org/repo/6d757z/images/1488249767-dblookup_export.png)

Example of results right click menu