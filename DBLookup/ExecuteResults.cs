﻿using System;
using System.Data;

namespace DBLookup
{
    public class ExecuteResults
    {
        public string StatusMessage { get; set; }

        public DataTable ResultsData { get; set; }

        public Exception ExceptionDetails { get; set; }
    }
}
