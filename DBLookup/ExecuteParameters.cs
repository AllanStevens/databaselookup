﻿namespace DBLookup
{
    public class ExecuteParameters
    {
        //private string provider { get; set; }

        public string SqlStatement { get; set; }

        public string ConnectionString { get; set; }
    }
}
